# Função para retornar o limite de crédito
def obter_limite():
    return salario * (idade / 1000) + 100


#Função para verificar o produto. Solicita e imprime produto, preço e nome completo.
def verificar_produto(limite):
    produto = str(input('Digite o produto '))
    preco = float(input('Digite o preço do produto '))
    nome_comp = str(input('Nome Completo: '))
    tamanho_nome_comp = len(nome_comp)
    primeir_nome = (nome_comp.split()[0])
    tamanho_primeiro = len(primeir_nome)
    print('Seu primeiro nome é: ', primeir_nome)
    print('Seu primeiro nome tem:', tamanho_primeiro, 'caracteres')
    print('Seu nome completo tem:', tamanho_nome_comp, 'caracteres')
    bloqueado = 0
    desconto = tamanho_primeiro
    preco_final = float(preco - desconto)

    # calcula se a compra será liberada, se vai poder ser parcelada ou se será bloqueada conforme o limite. Imprime para o usuário
    if preco <= (0.6 * limite):
        print('Liberado!')
    elif preco < (0.9 * limite):
        print('Liberado ao parcelar em até 2 vezes')
    elif preco < limite:
        print('Liberado ao parcelar em 3 ou mais vezes')
    else:
        print('bloqueado ')
        bloqueado = 1

     # calcula se haverá desconto, se houver faz o cálculo do desconto e imprime para o usuário
    if not bloqueado:
        if tamanho_nome_comp < preco < idade:
            print('O desconto foi de: %d reais' % desconto)
            print('Valor com desconto: %.2f' % preco_final)
        else:
            print('Não há desconto.')
    return limite - preco


#Apresentação
#solicita ao cliente o cargo, o salário, o ano de nascimento e calcula a idade
print('Seja bem-vindo a loja FRIENDS, aqui é a Bárbara Silva')
print('------------------------------------------------------')
print('Faremos sua análise de crédito, para isso precisameos de algumas informações.''\n')
cargo = str(input('Qual é o seu cargo atualmente? '))
salario = float(input('Qual é o seu salário atualmente? '))
ano = int(input('Qual é o seu ano de nascimento? '))
idade = 2021 - (ano)

limite = obter_limite()
print('O seu cargo é',cargo,'\nO seu salário é R$',salario,'\nE o seu ano de nascimento é',int(ano))
print('Sua idade é ', int(idade), 'anos')
print('O seu limite de crédito é R$ %.2f' %limite)
print('')

qtd_produtos = int(input('Quantos produtos você deseja cadastrar? '))

i = 1
soma = 0
limite_atual = limite
while i <= qtd_produtos:
    limite_atual = verificar_produto(limite_atual)
    print('O seu limite atualizado é %2.f' % limite_atual)
    i += 1
    soma += limite
